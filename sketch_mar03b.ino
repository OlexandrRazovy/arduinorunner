#include<U8glib.h>

U8GLIB_PCF8812 u8g(13, 11, 10, 9, 8);
int jumpBtn = 4;
int tonePin = 3;
bool isJump = false;
bool isPP = false;
bool isMM1 = false;
bool isMM2 = false;
bool oldState = false;
int i = 110;
int j = 110;
int cloud1I = 120;
int cloud2I = 160;
int jumpI = 0;
int gameOverI = 0;
int heroX = 10;
int heroY = 44;
int score = 0;
bool gameOver = false;
const uint8_t hero[] = {
  0x1C,
  0x3E,
  0x7E,
  0x7E,
  0x3C,
  0x10,
  0x7C,
  0x57,
  0xD1,
  0x10,
  0x38,
  0x28,
  0x6C,
  0x44,
  0x46,
  0xC2
};
const uint8_t rock[] = {
  0xAA,
  0xFF,
  0x7E,
  0x3C,
  0x24,
  0x24,
  0x3C,
  0x3C,
  0x3C,
  0x3C,
  0x7E,
  0xFF
};
const uint8_t cloud[] = {
    0x0,0xFC,0x0,0x0,
    0x1,0x3,0x3C,0x0,
    0x2,0x0,0xC3,0x0,
    0x4,0x0,0x80,0x80,
    0x4,0x0,0x0,0x80,
    0x4,0x0,0x0,0x40,
    0xC,0x0,0x0,0x70,
    0x30,0x0,0x0,0xC,
    0x20,0x0,0x0,0x4,
    0x40,0x0,0x0,0x2,
    0x40,0x0,0x0,0x2,
    0x40,0x0,0x0,0x2,
    0x20,0x0,0x0,0x4,
    0x1F,0xFF,0xFF,0xF8
};
void setup() {
  u8g.setRot180();
  u8g.setFont(u8g_font_unifont);
  pinMode(jumpBtn,INPUT);
  pinMode(tonePin,OUTPUT);
  Serial.begin(9600);
}

const char *convert_score(int sc) {
  String s = String(sc);
  static char buf[3];
  s.toCharArray(buf,3);
  return buf;
}

void draw() {
  if(gameOver) {
    u8g.setFont(u8g_font_unifont);
    u8g.drawStr(16,10,"Game Over");
    u8g.setFont(u8g_font_gdr25r);
    u8g.drawStr(42,40,convert_score(score));
  } else {
    u8g.drawBox(0,60,101,4);
    u8g.drawBitmap(heroX,heroY,1,16,hero);
    u8g.drawBitmap(i,48,1,12,rock);
    u8g.drawBitmap(j,48,1,12,rock);
    u8g.drawStr(10,15,convert_score(score));
    u8g.drawBitmap(cloud1I,10,4,14,cloud);
    u8g.drawBitmap(cloud2I,15,4,14,cloud);
  }
}

void loop() {
  // picture loop
  u8g.firstPage();  
  do {
    draw();
  } while( u8g.nextPage() );
  if(!gameOver) {
    i--;
    j--;
    cloud1I--;
    cloud2I--;
  } else {
    i = 101;
    j = 101;
  }

  randomSeed(analogRead(random(0,6)));
  if(i < 0) i = random(110,126);
  if(i <= heroX+8 && i >= heroX && isJump != 1) {
    gameOver = true;
    tone(3,50,150);
  } else if(i <= heroX+8 && i >= heroX && isJump == 1 && !isMM1) {
    score++;
    tone(tonePin,600,150);
    isMM1 = true;
  }

  randomSeed(analogRead(random(0,6)));
  if(j < 0) j = random(134,154);
  if(j <= heroX+8 && j >= heroX && isJump != 1) {
    gameOver = true;
    tone(3,50,150);
  } else if(j <= heroX+8 && j >= heroX && isJump == 1 && !isMM2) {
    score++;
    tone(tonePin,600,150);
    isMM2 = true;
  }

  if(cloud1I < -32) cloud1I = 120 + random(0,10);
  if(cloud2I < -32) cloud2I = 160 + random(0,30);
  
  if(digitalRead(jumpBtn) == 1 && gameOver && gameOverI > 5) {
    asm volatile ("  jmp 0");
  }
  
  if(digitalRead(jumpBtn) == 1 && !oldState) {
    Serial.println("jump");
    isJump = true;
    oldState = true;
  } else if(jumpI >= 20) {
    isJump = false;
    oldState = false;
    isMM1 = false;
    isMM2 = false;
  }

  if(gameOver) gameOverI++;
  if(isJump) jumpI++;
  if(isJump && !isPP) {
    heroY -= 10;
    isPP = true;
  } else if(!isJump && isPP) {
    heroY += 10;
    isPP = false;
    jumpI = 0;
  }
  // rebuild the picture after some delay
  delay(1);
}

